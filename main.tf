resource "aws_security_group" "jenkins_sg" {
  name        = "Jenkins-Security Group"
  description = "Open 22,443,80,8080"

  ingress = [
    for port in [22, 80, 443, 8080] : {
      description      = "TLS from VPC"
      from_port        = port
      to_port          = port
      protocol         = "tcp"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = []
      prefix_list_ids  = []
      security_groups  = []
      self             = false
    }
  ]

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "jenkins_sg"
  }
}

resource "aws_instance" "web" {
  ami                    = "ami-0f5ee92e2d63afc18"  # Change the AMI if in a different region
  instance_type          = "t2.medium"
  key_name               = "Mumbai"   # Change to the key pair name in AWS
  vpc_security_group_ids = [aws_security_group.jenkins_sg.id]
  user_data              = templatefile("./install_jenkins.sh", {})

  tags = {
    Name = "Jenkins-sonar"
  }

  root_block_device {
    volume_size = 8
  }
}
